# To read the rules of this project, please got to http://www.blackwasp.co.uk/passwordstrength.aspx

import string
import getpass
import random

# Random password generator function
def pass_gen(score):
    lower = string.ascii_lowercase
    upper = string.ascii_uppercase
    digit = string.digits
    punc = string.punctuation

    new_pass_sample = lower + upper + digit + punc
    new_pass_length = 16
    new_password = "".join(random.sample(new_pass_sample, new_pass_length))

    if score < 80:
        print(f"Your recommended new passwords is : {new_password}")


# To get password input from the user for checking
password = getpass.getpass("Password input:")
pw_length = len(password)
score = pw_length * 6
if score > 60:
    score = 60
# To remove whitespaces inside the password
password.replace(" ", "")

# To check if passwords met the criteria, credits to Ayman 
UpperCase=any([1 if c in string.ascii_uppercase else 0 for c in password])
LowerCase=any([1 if c in string.ascii_lowercase else 0 for c in password])
SpecialChar=any([1 if c in string.punctuation else 0 for c in password])
Digits=any([1 if c in string.digits else 0 for c in password])


for passChar in password:
    if passChar.isupper():
        UpperCase += 1
    elif passChar.islower():
        LowerCase += 1
    elif passChar in  string.punctuation:
        SpecialChar += 1
    elif passChar.isdigit():
        Digits += 1
criteria=[UpperCase,LowerCase,SpecialChar,Digits]



# Iterate over the list to check if criteria is met
for x in criteria:
    if x == 1:
        score += 5
    elif x >= 2:
        score += 10
print(f"Your password score is {score}")

if score < 50 :
    print("Your password is unacceptable and should be changed!")
if score < 60 and score > 49:
    print("Your password is weak, changing your password is recommended.")
if score < 80 and score > 59:
    print("Your password is OK.")
if score < 100 and score > 79:
    print("Your password is strong!")
if score == 100:
    print("Your password is secured!!!")

pass_gen(score)